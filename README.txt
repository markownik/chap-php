Challenge-Response Login System
Version 1.0
Author: Marek Biedrzycki
--------------------------------------------------------------------------------
This is an implementation of the Alternative Challenge-Response
login/registration system, as described by Paul Johnston here:
http://pajhome.org.uk/crypt/md5/auth.html

The main purpose of this challenge-response is to provide a way to authenticate
users without transmitting their passwords in plaintext.  Obviously, if an
SSL/HTTPS connection is available, this problem has already been solved.
However, for those unable or unwilling to obtain a SSL certificate for
this purpose, challenge-response may improve the security of the authentication
process.

I won't repeat what has already been explained by Paul Johnston at the
previously-mentioned link.  He gives a great explanation of how the system works
and what its weaknesses are.  Reading it, especially the `Alternative System`
section, will give you a thorough understanding of the protocol used here.

Installation and Usage
--------------------------------------------------------------------------------
Simply copy the entire `/src` folder to a directory on your server.  In the
Open your browser and point it to the `/src/demo/index.php` file to see the
system in action.  You can login and register new users, who are stored in a
simple file-based store for the purposes of the demo.

All of the source files are thoroughly documented/commented so that you can
modify the code to suit your needs.  Most of you will probably want to configure
the system to work with a MySQL-based store, and that's fairly straightforward
to do since interfaces have been provided.  You merely need to write your own
implementation of `ChapDataStore`, providing all the required methods and
behaviour.  I was hesistant to provide a concrete database-based implemenation
considering that many people are using PHP frameworks or other libraries that
abstract database access using their own APIs.

JavaScript is used on the client side to compute the challenge-responses.  The
`/src/main/chap.js` file contains all of this code.  The
`/src/main/lib/sha1.js1` file is from Paul Johnston and provides the necessary
crytographic hash functions in JavaScript, specifically the SHA-1 digest
algorithm.  Additionally, the jQuery (http://jquery.com) library is used and is
provided.

Weaknesses and Drawbacks
--------------------------------------------------------------------------------
Besides the obvious weaknesses (like leaking of information
regarding when a particular user has logged in), the system also has the
following disadvantages:

- Cannot enforce a password complexity policy server-side
This occurs because during registration, the plaintext password is not sent
to the server.  Instead, a response based on the password and a challenge
string is sent and stored server-side, to be used for authentication.

Since the plaintext password cannot be obtained from the response, it is
impossible to enforce a password complexity policy.  (The responses are
message digests that are fixed-length strings)  At the very most, the
response can only be checked to see that it was not generated from a
blank password.  More extensive checking will have to be done client-side,
where it can be circumvented.

Requirements
--------------------------------------------------------------------------------
- PHP 5 (PHP 5.2.5 tested)
- jQuery (jQuery 1.2.3 included)

Known Issues
--------------------------------------------------------------------------------
- Cannot use an id attribute of "submit" on any input elements in the
  login/registration form.  (Weird jQuery/JavaScript bug?)

Todo:
--------------------------------------------------------------------------------
- Only tested on Apache 2.2/PHP 5.2.5.