<?php
require_once('exceptions/ChapAuthenticationException.php');

/**
 * Specifies the operations required of a modified CHAP login system.
 *
 * The exact protocol used is described in the section "Alternative System" at
 * the following URL: http://pajhome.org.uk/crypt/md5/auth.html
 *
 * Note that some functionality of the system is internal, such as the
 * generation of challenges, and thus cannot be specified in this interface.
 *
 * @author Marek Biedrzycki
 * @copyright Copyright (c) 2016, Marek Biedrzycki
 * @license http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link 
 */
Interface ChapAuthentication
{
  /**
   * Returns the random challenge strings for a specified user in an
   * associative array.
   *
   * The array should have the challenge values associated with the keys
   * 'challenge1' and 'challenge2'.
   *
   * If the user does not exist, unique and deterministic challenges should
   * be returned for the username supplied.  This ensures that information
   * is not leaked about which usernames are available.  The easiest way
   * to accomplish this is to return `hash(username + server secret)` when
   * the username does not exist. The `hash()` function should be the same as
   * what was used to generate the challenges.
   *
   * @param string $username the username of the account.
   * @return array an associative array with the keys 'challenge1' and
   *         'challenge2'.
   */
  function getChallenges($username);

  /**
   * Generates and returns a new random challenge.
   *
   * This is needed during the registration process, as the client needs a
   * random challenge to work with.
   *
   * @return string the new random challenge.
   */
  function getNewChallenge();

  /**
   * Performs authentication on a user.
   *
   * The $password variable should be set to the response to the challenge.
   *
   * @param string $username the username of the account.
   * @param string $password the response from the user attempting to
   *               authenticate.
   * @param string $responseNext the value from the client to update the
   *        password field with for subsequent logins.
   * @throws ChapAuthenticationException if the user could not be authenticated.
   */
  function authenticateUser($username, $password, $responseNext);

  /**
   * Authenticates a user.
   *
   * The $password variable in this case is a plaintext password.
   *
   * @param string $username the username of the account.
   * @param string $password the supplied plaintext password.
   * @throws ChapAuthenticationException if the user could not be authenticated.
   */
  function authenticateUserPlaintext($username, $password);

  /**
   * Registers a new user.
   *
   * All three parameters are expected to be supplied by the client. The
   * $password variable should be set to the response computed using
   * $challenge1.  An implementation should generate a new `challenge2` value
   * and store it along with the user's record.
   *
   * @param string $username the username to register.
   * @param string $password the response from the user, which should have
   *        been computed from their plaintext password and the challenge.
   * @param string $challenge1 the challenge used compute the response.
   * @throws ChapAuthenticationException if the user could not be registered.
   */
  function registerUser($username, $password, $challenge1);

  /**
   * Registers a new user using a plaintext password, as opposed to the CHAP
   * scheme defined before.
   *
   * @param string $username the username to register.
   * @param string $password the plaintext password supplied.
   * @throws ChapAuthenticationException if the user could not be registered.
   */
  function registerUserPlaintext($username, $password);

}
?>