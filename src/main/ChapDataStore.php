<?php
require_once('exceptions/ChapDataStoreException.php');

/**
 * Provides an easy way for a ChapAuthentication object to read and write
 * users' data.
 *
 * Using this provides abstraction and separation of functionality, by
 * allowing an implemenation of ChapAuthentication to focus on the particulars
 * of CHAP login, while ChapDataStore takes care of how the data is exactly
 * stored.
 *
 * @author Marek Biedrzycki
 * @copyright Copyright (c) 2016, Marek Biedrzycki
 * @license http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link 
 */
Interface ChapDataStore
{
  /**
   * Returns the user's record.
   *
   * This method should return an associative array with AT LEAST the following
   * keys: `username`, `password`, `challenge1`, `challenge2`.
   *
   * @param string $username the username of the account.
   * @returns array an associative array containing the user's details.
   * @throws ChapDataStoreException if the username was not found.
   */
  function getUserData($username);

  /**
   * Adds a new user to the store.
   *
   * The supplied $user associative array should have AT LEAST the following
   * keys set: `username`, `password`, `challenge1`, `challenge2`.
   *
   * @param array $user an associative array with the user's details.
   * @throws ChapDataStoreException if the username already exists or if the the
   *         account could otherwise not be created.
   */
  function addUser($user);

  /**
   * Update's a user's record.
   *
   * The supplied $user associative array should have AT LEAST the following
   * keys set: `username`, `password`, `challenge1`, `challenge2`.
   *
   * @param array $user an associative array with the user's details.
   * @throws ChapDataStoreException if the username was not found.
   */
  function updateUser($user);

}
?>