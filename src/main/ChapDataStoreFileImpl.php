<?php
require_once('ChapDataStore.php');

/**
 * A simple file-based storage of users' data for demo/test purposes.
 *
 * {@inheritdoc}
 *
 * This class is provided only for demo purposes, as you'd probably want to
 * write your own implementation that uses a database backend.
 *
 * The list of users is stored in a text file of comma-separated values, one
 * user per line.  The format is `username,password,challenge1,challenge2`.
 * An example entry is:
 *
 * marek,b31f8012b6428d70da9a283e6a8f5c7e1c5a00e4,3c1ac1deb549c81940ca3f3ac3c6c6479e769ccb,a9be62ecdd49f64e322ce40b00d67f54b01b6d6b
 *
 * I am hesistant to provide a concrete database implementation, as some
 * developers might want to use this in the context of a framework where
 * database access is abstracted.  By simply providing an interface, any
 * developer can write their own implementation, thus deciding whether they
 * want to use abstracted database access or direct SQL queries.
 *
 * @author Marek Biedrzycki
 * @copyright Copyright (c) 2016, Marek Biedrzycki
 * @license http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link 
 */
Class ChapDataStoreFileImpl implements ChapDataStore
{
  /**
   * The path to the file that stores the users' data.
   *
   * @var string
   */
  private $userDataFilePath;

  /**
   * Stores the user's data in an associative array.
   *
   * @var array
   */
  private $users = array();

  /**
   * Constructor accepts path to the file.
   *
   * @param string $userDataFilePath path to the file.
   * @throws Exception if the file does not exist.
   */
  public function __construct($userDataFilePath)
  {
    if (!file_exists($userDataFilePath))
    {
      throw new ChapDataStoreException("Supplied file '$userDataFilePath' does not exist.");
    }

    $this->userDataFilePath = $userDataFilePath;
    $this->loadUserData();
  }

  /**
   * Loads user data from the file.
   *
   * Data should be stored in the following format, one user per line.
   *
   * username, password,challenge1,challenge2
   *
   * @throws Exception if data could not be read properly.
   */
  private function loadUserData()
  {
    $this->users['username'] = array();
    $this->users['password'] = array();
    $this->users['challenge1'] = array();
    $this->users['challenge2'] = array();

    $handle = @fopen($this->userDataFilePath, 'r');

    if (!$handle)
    {
      throw new ChapDataStoreException("Could not read from file.");
    }

    while (!feof($handle))
    {
      $line = fgets($handle);
      $line = trim($line);

      // Ignore blank lines.
      if (empty($line))
      {
        continue;
      }

      $data = explode(',', $line);

      if (count($data) != 4)
      {
        throw new ChapDataStoreException('Could not read user data.');
      }

      $this->users['username'][] = trim($data[0]);
      $this->users['password'][] = trim($data[1]);
      $this->users['challenge1'][] = trim($data[2]);
      $this->users['challenge2'][] = trim($data[3]);
    }

    fclose($handle);
  }

  /**
   * {@inheritdoc}
   */
  public function getUserData($username)
  {
    $position = $this->getUserPosition($username);

    $user = array();
    $user['username'] = $this->users['username'][$position];
    $user['password'] = $this->users['password'][$position];
    $user['challenge1'] = $this->users['challenge1'][$position];
    $user['challenge2'] = $this->users['challenge2'][$position];

    return $user;
  }

  /**
   * Gets the user's position in the array.
   *
   * @param string $username
   * @return integer the user's position.
   * @throws Exception if the username does not exist.
   */
  private function getUserPosition($username)
  {
    // Note that if there are multiple same usernames, this returns just the
    // first one.
    $position = array_keys($this->users['username'], $username);

    if (empty($position))
    {
      throw new ChapDataStoreException('Username not found');
    }

    return $position[0];
  }

  /**
   * {@inheritdoc}
   */
  public function addUser($user)
  {
    if (empty($user))
    {
      throw new ChapDataStoreException('Invalid argument');
    }

    // Strip out any commas.
    $user = str_replace(',', '', $user);

    // Verify validity of data.
    $this->checkUserData($user);

    // Check if the username already exists.
    if (in_array($user['username'], $this->users['username']))
    {
      throw new ChapDataStoreException('Username already exists');
    }

    // Add the new user to the list and then write to storage.
    $this->users['username'][] = $user['username'];
    $this->users['password'][] =  $user['password'];
    $this->users['challenge1'][] = $user['challenge1'];
    $this->users['challenge2'][] = $user['challenge2'];

    $this->writeUserData();
  }

  /**
   * {@inheritdoc}
   */
  public function updateUser($user)
  {
    if (empty($user))
    {
      throw new ChapDataStoreException('Invalid argument');
    }

    // Strip out any commas.
    $user = str_replace(',', '', $user);

    // Verify validity of data.
    $this->checkUserData($user);

    // Get the user's position in the array.
    $position = $this->getUserPosition($user['username']);

    $this->users['username'][$position] = $user['username'] ;
    $this->users['password'][$position] =  $user['password'];
    $this->users['challenge1'][$position] = $user['challenge1'];
    $this->users['challenge2'][$position] = $user['challenge2'];

    $this->writeUserData();
  }

  /**
   * Writes the current user data to storage.
   */
  private function writeUserData()
  {
    // Form the contents for the user data.
    $contents = '';
    foreach ($this->users['username'] as $pos => $username)
    {
      $contents .= $username . ',' . $this->users['password'][$pos] . ',' .
        $this->users['challenge1'][$pos] . ',' . $this->users['challenge2'][$pos] . "\n";
    }

    $handle = @fopen($this->userDataFilePath, 'w');

    if (!$handle)
    {
      throw new ChapDataStoreException("Could not read from file.");
    }

    if (fwrite($handle, $contents) === false)
    {
      throw new ChapDataStoreException("Could not write to file.");
    }

    fclose($handle);
  }

  /**
   * Verifies that a user's data is valid before saving to storage.
   */
  private function checkUserData($user)
  {
    if (empty($user['username']))
    {
      throw new ChapDataStoreException('Invalid username');
    }
    else if (empty($user['password']))
    {
      throw new ChapDataStoreException('Invalid password');
    }
    else if (empty($user['challenge1']))
    {
      throw new ChapDataStoreException('Invalid challenge');
    }
    else if (empty($user['challenge2']))
    {
      throw new ChapDataStoreException('Invalid challenge');
    }
  }
}
?>