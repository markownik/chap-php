<?php
/**
 * Base type for all exceptions thrown by ChapAuthentication.
 *
 * This could be extended to provide a finer level of detail.
 *
 * @author Marek Biedrzycki
 * @copyright Copyright (c) 2016, Marek Biedrzycki
 * @license http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link 
 */
class ChapAuthenticationException extends Exception
{
   public function __construct($message = null, $code = 0)
   {
       // Make sure everything is assigned properly.
       parent::__construct($message, $code);
   }
}

?>
