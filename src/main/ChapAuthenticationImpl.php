<?php
require_once('ChapAuthentication.php');

/**
 * Simple implementation of @link ChapAuthentication.
 *
 * {@inheritdoc}
 *
 * @author Marek Biedrzycki
 * @copyright Copyright (c) 2016, Marek Biedrzycki
 * @license http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link 
 */
Class ChapAuthenticationImpl implements ChapAuthentication
{
  /**
   * Holds the object used to access the user datastore.
   *
   * @var ChapDataStore
   */
  private $chapDataStore;

  /**
   * Constructor should be initialized with a @link ChapDataStore object.
   *
   * @param ChapDataStore $chapDataStore the object representing the user
   *        datastore.
   */
  public function __construct(ChapDataStore $chapDataStore)
  {
    $this->chapDataStore = $chapDataStore;
  }

  /**
   * {@inheritdoc}
   */
  public function getChallenges($username)
  {
    // If empty, don't return any challenges.
    if (empty($username))
    {
      return array('challenge1' => '', 'challenge2' => '');
    }

    $challenges = array();
    try
    {
      $userData = $this->chapDataStore->getUserData($username);

      $challenges['challenge1'] = $userData['challenge1'];
      $challenges['challenge2'] = $userData['challenge2'];
    }
    // User does not exist; return some challenge values based on the username
    // supplied, so that the behaviour is indistinguishable from the case of
    // a username actually existing.
    catch (ChapDataStoreException $e)
    {
      $challenges['challenge1'] = $this->passwordTransform(
        'some server secret part I' . $username);

      $challenges['challenge2'] = $this->passwordTransform(
        'another server secret - GET TO DA CHOPPA!' . $username);
    }

    return $challenges;
  }

  /**
   * Generates and returns a new challenge based on the SHA-1 digest
   * algorithm for use in CHAP systems.
   *
   * {@inheritdoc}
   *
   * @return string the SHA-1 160-bit challenge, converted to a 40-character
   *         hex string.
   */
  public function getNewChallenge()
  {
    return hash_hmac('sha1', mt_rand(), time());
  }

  /**
   * {@inheritdoc}
   */
  public function authenticateUser($username, $password, $responseNext)
  {
    // Validate inputs.
    if (empty($username))
    {
      throw new ChapAuthenticationException('Invalid argument');
    }
    else if ($this->getChallengeLength() != strlen($password))
    {
      throw new ChapAuthenticationException('Invalid argument');
    }
    else if ($this->getChallengeLength() != strlen($responseNext))
    {
      throw new ChapAuthenticationException('Invalid argument');
    }

    try
    {
      // Try to get the user's details.
      $userData = $this->chapDataStore->getUserData($username);
    }
    catch (ChapDataStoreException $e)
    {
      // User does not exist, so deny authentication.
      throw new ChapAuthenticationException($e->getMessage());
    }

    // Verify credentials.
    if ($this->passwordTransform($password) == $userData['password'])
    {
      // Authentication successful.

      // Update the user's details.  This involves setting the `password` to the
      // challenge-response from the client, updating the challenge to be used,
      // and generating a new one for subsequent logins.
      $userData['password'] = $responseNext;
      $userData['challenge1'] = $userData['challenge2'];
      $userData['challenge2'] = $this->getNewChallenge();

      $this->chapDataStore->updateUser($userData);
    }
    else
    {
      // Invalid credentials supplied if the passwords did not match.
      throw new ChapAuthenticationException('Invalid password');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function authenticateUserPlaintext($username, $password)
  {
    // Validate inputs.
    if (empty($username))
    {
      throw new ChapAuthenticationException('Invalid argument');
    }
    else if (empty($password))
    {
      throw new ChapAuthenticationException('Invalid argument');
    }

    try
    {
      // Try to get the user's details.
      $userData = $this->chapDataStore->getUserData($username);
    }
    catch (ChapDataStoreException $e)
    {
      // User does not exist, so deny authentication.
      throw new ChapAuthenticationException($e->getMessage());
    }

    // Verify credentials.  We must compute the hashed challenge-response here,
    // since the client did not use JavaScript to do this.
    if ($this->passwordPlainTextTransform($password, $userData['challenge1'])
      == $userData['password'])
    {
      // Authentication successful.

      // NOTE: There is no need to update the user's details with new challenges
      // like was done for #authenticateUser().  This is because CHAP was not
      // used and the password was transmitted in plaintext anyway.

      // This `if` block has been left in in the event that there are
      // additional actions to be carried out after successful login.
    }
    else
    {
      // Invalid credentials supplied if the passwords did not match.
      throw new ChapAuthenticationException('Invalid password');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerUser($username, $password, $challenge1)
  {
    // Verify the length of the $password and $challenge1 hashes.  You may
    // also want to check other restrictions on the $username during
    // registration.
    if (empty($username))
    {
      throw new ChapAuthenticationException('Invalid argument');
    }
    else if ($this->getChallengeLength() != strlen($password))
    {
      throw new ChapAuthenticationException('Invalid argument');
    }
    else if ($this->getChallengeLength() != strlen($challenge1))
    {
      throw new ChapAuthenticationException('Invalid argument');
    }
    // Check if the plaintext password was empty.
    else if ($this->passwordPlainTextTransform('', $challenge1) == $password)
    {
      throw new ChapAuthenticationException('Password cannot be empty');
    }

    $userData = array();
    $userData['username'] = $username;
    $userData['password'] = $password;
    $userData['challenge1'] = $challenge1;
    $userData['challenge2'] = $this->getNewChallenge();

    // Add the new user to the data store.
    try
    {
      $this->chapDataStore->addUser($userData);
    }
    catch (ChapDataStoreException $e)
    {
      throw new ChapAuthenticationException($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerUserPlaintext($username, $password)
  {
    // Validate inputs.
    if (empty($username))
    {
      throw new ChapAuthenticationException('Invalid argument');
    }
    else if (empty($password))
    {
      throw new ChapAuthenticationException('Invalid argument');
    }

    $userData = array();
    $userData['username'] = $username;

    // Generate two new challenges.
    $userData['challenge1'] = $this->getNewChallenge();
    $userData['challenge2'] = $this->getNewChallenge();

    // Compute the hashed challenge-response based on the first challenge
    // and the password.
    $userData['password'] = $this->passwordPlainTextTransform(
      $password,
      $userData['challenge1']
    );

    // Add the new user to the data store.
    try
    {
      $this->chapDataStore->addUser($userData);
    }
    catch (ChapDataStoreException $e)
    {
      throw new ChapAuthenticationException($e->getMessage());
    }
  }

  /**
   * Defines the 'transformation' or hashing function used to compare the
   * supplied password-response with the stored one.
   *
   * @param string $password the supplied password value from the client.
   */
  private function passwordTransform($password)
  {
    return sha1($password);
  }

  /**
   * Computes the hashed-response of a plaintext password and a challenge.
   *
   * Normally this would be done on the client, if JavaScript was enabled.
   * However, in the case that it was not, and the plaintext password was
   * submitted instead, we need to compute the response server-side, since
   * this is what has been stored.
   *
   * @param string $password the plaintext password.
   * @param string $challenge the challenge string.
   * @return string the hashed response.
   */
  private function passwordPlainTextTransform($password, $challenge)
  {
    // Compute the password-challenge-response; normally this would have been
    // done on the client but if JavaScript is disabled the plaintext password
    // is instead passed in.
    $passwordChallengeResponse = hash_hmac(
      'sha1',
      $challenge,
      $password
    );

    return $this->passwordTransform($passwordChallengeResponse);
  }

  /**
   * Gets the length of the challenges currently being used.
   *
   * This assumes that all challenges are the same length, which should be the
   * case if using hash function.
   *
   * @return int the length of the challenges in use.
   */
  private function getChallengeLength()
  {
    return strlen($this->getNewChallenge());
  }

}
?>