<?php
/**
 * An example of the modified CHAP PHP login system and how to use it.
 *
 * All basic components of the modified CHAP login system in one file:
 * 1) Authenticating a user after submitting credentials.
 * 2) Retrieving a user's challenges for the client to work with.
 * 3) Registering a user.
 * 4) Presenting the login page.
 *
 * Authentication and registration will work with or without JavaScript;
 * without JavaScript, the process "steps down" to a less secure method where
 * passwords are transmitted plaintext and CHAP is not used.  You can choose to
 * not allow this in your implementation, but I've made such methods available
 * in the event that you wish to be user-friendly to all clients.
 *
 * Uses a file-based store, as defined in the `users.txt` file.
 *
 * @author Marek Biedrzycki
 * @copyright Copyright (c) 2016, Marek Biedrzycki
 * @license MIT
 */

require_once('../main/ChapAuthenticationImpl.php');
require_once('../main/ChapDataStoreFileImpl.php');

// Create a new ChapAuthentication object using the file-based data store.
$chap = new ChapAuthenticationImpl(new ChapDataStoreFileImpl('users.txt'));

$backMessage = '<p><a href="index.php">Back</a></p>';

/*--- Credentials submitted; proceed to verify. ---*/
if (!empty($_POST['action'])
  && 'login' == $_POST['action']
  && !empty($_POST['username']))
{
  $username = $_POST['username'];

  /*
   * Note: I have decided to use exception handling for the authentication
   * process instead of returning `true` or `false` for the result.
   *
   * There are a few things to note about this:
   *
   * 1) Currently, all invalid authentications result in a generic
   * ChapAuthenticationException being thrown.  This could be refined further
   * so that sub-types of ChapAuthenticationException are thrown for different
   * reasons, such as "username not found" or "bad password".  This would allow
   * you to handle each differently in the calling code.
   *
   * 2) Alternatively, if you only care about true/false, you can just treat
   * the authentication process like I have done below.  In most situations,
   * you wouldn't want to reveal to users *why* the login failed, just that
   * it *did* fail.  However, with the usage of exceptions, the flexibility
   * of having this ability is retained.
   */
  try
  {
    if (!empty($_POST['password']) && !empty($_POST['response-next']))
    {
      // CHAP supported by client: JavaScript was enabled.
      $password = $_POST['password'];
      $responseNext = $_POST['response-next'];

      $chap->authenticateUser($username, $password, $responseNext);
    }
    else if (!empty($_POST['password-plaintext']))
    {
      // CHAP not supported by client: JavaScript probably not used.
      // If the 'password' and 'response-next' parameters were not submitted,
      // we can attempt to authenticate using the plaintext password.
      $passwordPlaintext = $_POST['password-plaintext'];

      $chap->authenticateUserPlaintext($username, $passwordPlaintext);
    }

    // Authentication successful if we got this far.

    // Here is where you would carry out actions after a successful login,
    // i.e. setting a session cookie to track the authenticated user.

    echo '<h1>Authentication successful!</h1>';
    echo '<p><strong>Note</strong>: If you do a straight refresh on this page, submitting the '
      . 'same POST data as before, authentication <strong>will fail</strong>. '
      . 'This is because the challenge strings were updated for a successful '
      . 'login and the old ones cannot be used, thus preventing a replay attack.';
  }
  catch (ChapAuthenticationException $e)
  {
    // Authentication failed if an exception was thrown.
    echo '<h1>Authentication FAILED</h1>';

    // Normally, you would NOT want to divulge this information.
    echo '<p>' . $e->getMessage() . '</p>';
  }

  // Debug: See what was actually submitted to the server.
  printPostData();

  exit($backMessage);
}

/*--- Registration of a new user. ---*/
else if (!empty($_POST['action'])
  && 'register' == $_POST['action']
  && !empty($_POST['username']))
{
  $username = $_POST['username'];

  // The registration process will throw an exception if it fails.
  try
  {
    if (!empty($_POST['password']) && !empty($_POST['challenge1']))
    {
      // CHAP supported by client: JavaScript was enabled.
      $password = $_POST['password'];
      $challenge1 = $_POST['challenge1'];

      $chap->registerUser($username, $password, $challenge1);
    }
    else if (!empty($_POST['password-plaintext']))
    {
      // CHAP not supported by client: JavaScript probably not used.
      // If the 'password' and 'challenge1' parameters were not submitted,
      // we can register the user using the provided plaintext password.
      $passwordPlaintext = $_POST['password-plaintext'];

      $chap->registerUserPlaintext($username, $passwordPlaintext);
    }

    // Registration successful if we got this far.

    // You may want to carry out other actions after registration, i.e.,
    // automatically authenticating the user for the new account and setting
    // a session cookie.

    echo '<h1>Registration successful!</h1>';
  }
  catch (ChapAuthenticationException $e)
  {
    // Registration failed if an exception was thrown.
    echo '<h1>Registration FAILED</h1>';
    echo '<p>' . $e->getMessage() . '</p>';
  }

  // Debug: See what was actually submitted to the server.
  printPostData();

  exit($backMessage);
}

/*--- Return challenges for the username in JSON. ---*/
else if (!empty($_GET['action'])
  && 'challenges' == $_GET['action']
  && !empty($_GET['username']))
{
  $challenges = $chap->getChallenges($_GET['username']);

  // You might want to use a utility method that converts associative arrays
  // to JSON strings.
  echo "{'c1':'$challenges[challenge1]','c2':'$challenges[challenge2]'}";

  exit();
}

/*--- Return a random new challenge, used during registration. ---*/
else if (!empty($_GET['action'])
  && 'challenge' == $_GET['action'])
{
  $challenge = $chap->getNewChallenge();

  echo "{'c1':'$challenge'}";

  exit();
}

/*--- Default: Supply with login/registration form. ---*/
else
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>CHAP Login System</title>
    <script src="../main/lib/jquery.min.js" type="text/javascript"></script>
    <script src="../main/lib/sha1.js" type="text/javascript"></script>
    <script src="../main/chap.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="style.css" />
  </head>

  <body>
    <h1>CHAP Login System</h1>
    <form class="user" id="login" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
      <fieldset>
        <legend>Login</legend>

        <em>Initial login/password is 'peter'/'tester'</em>

        <dl>
        <dt><label for="username">Username:</label></dt>
        <dd><input type="text" name="username" id="username" size="30" /></dd>
        <dt><label for="password-plaintext">Password:</label></dt>
        <dd><input type="password" name="password-plaintext" id="password-plaintext" size="30" /></dd>
        </dl>

        <input type="hidden" name="action" value="login" />

        <div class="submit"><input type="submit" value="Sign In" /></div>
        <span class="ajax-loading hide"><img src="indicator.gif" alt="" /> Loading...</span>

      </fieldset>
    </form>

    <form class="user" id="register" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
      <fieldset>
        <legend>Register</legend>

        <em>Create a new user - can then login above</em>

        <dl>
        <dt><label for="username-register">Username:</label></dt>
        <dd><input type="text" name="username" id="username-register" size="30" /></dd>
        <dt><label for="password-plaintext-register">Password:</label></dt>
        <dd><input type="text" name="password-plaintext" id="password-plaintext-register" size="30" /></dd>
        </dl>

        <input type="hidden" name="action" value="register" />

        <div class="submit"><input type="submit" value="Register" /></div>
        <span class="ajax-loading hide"><img src="indicator.gif" alt="" /> Loading...</span>

      </fieldset>
    </form>

    <p id="javascript-disabled" class="warning">
    Please enable JavaScript for a more secure login.
    </p>

    <h2>Explanation</h2>
    <p>
    A demo of the modified CHAP login/registration system
    <a href="http://pajhome.org.uk/crypt/md5/auth.html">described by Paul
    Johnston</a>.  This provides a way to authenticate users without transmitting
    passwords in plaintext, in the event that SSL/HTTPS is not available.  It
    is not intended to replace HTTPS.
    </p>
    <p>
    In this system, the password is <em>never</em> transmitted
    in plaintext, either during login or registration.  Instead, a response that
    is based on a random challenge string and the password is sent to the
    server; this is used to determine if the password was valid, without having
    to transmit the password itself in plaintext.  Each time a user logs in, the
    challenge strings are updated so that the required response for the next
    login will be different, preventing sniffing/replay attacks from being
    effective.  Only someone who knows the plaintext password can compute the
    correct response to the challenge strings each time.
    </p>
    <p>
    A similar method is used to
    get the password during registration.  Passwords are <strong>not</strong>
    stored as plaintext on the server-side either.  (Feel free to use a tool
    like <a href="https://addons.mozilla.org/en-US/firefox/addon/3829">Live HTTP
    Headers</a> to see <em>exactly</em> what is submitted to make sure no
    plaintext passwords are submitted.)
    </p>
    <p>
    This particular demo is also able to `step down` in the case that JavaScript is not
    enabled/available on the client side.  Because JavaScript is needed to
    compute the challenge-responses essential to the CHAP system, CHAP will not
    be available if JavaScript is not.  However, in this situation, plaintext
    passwords <em>will</em> be transmitted to the server during authentication.
    This is obviously less secure, but is a tradeoff between accessibility and
    security.  (Note that most websites you login to use plaintext password
    transmission, but may already be properly secured via SSL)  In your
    implementation, you can choose whether you want to allow these `insecure`
    logins.
    </p>
    <p>
    This system is not without weaknesses; in particular, it leaks information
    about when a user has logged in and thus can confirm if a user account
    exists or not.  A full discussion of these weaknesses is beyond the
    scope of this demo (and my knowledge), so I recommend that you
    <a href="http://pajhome.org.uk/crypt/md5/auth.html">read Paul Johnston's
    tutorial on the CHAP login system</a>, especially the information under
    the `Alternative System` heading.
    </p>

    <p class="footnote">
      <small>
        Copyright &copy; 2016 <a href="http://unitstep.net">Marek Biedrzycki</a>.
        Distributed under the MIT License.  See the included LICENSE.txt file
        for more details.<br />
        Parts of code, concepts and ideas copyright &copy;
        <a href="http://pajhome.org.uk/crypt/index.html">Paul Johnston</a>.
      </small>
    </p>
  </body>
</html>
<?
}

/**
 * Outputs what the client submitted to the server using HTTP POST.
 */
function printPostData()
{
  echo '<p>What was submitted to the server:</p>';
  echo '<pre>';
  print_r($_POST);
  echo '</pre>';
}

?>